﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

/*
 * Maily keeps track of the enemies - and also match settings
 * 
*/
public class EnemyManager : NetworkBehaviour {
    private const string ENEMY_ID_PREFIX = "Enemy ";

    public static Dictionary<string, Enemy> enemies = new Dictionary<string, Enemy>();
    public static Dictionary<string, Enemy> spawnableEnemies = new Dictionary<string, Enemy>();
    public List<Enemy> spawnableEnemyList;
    [HideInInspector]
    public Pathfinder pathfinder;
    Vector3 playerBase;
    Vector3 spawnPos;
    public static List<Vector3> groundPath;
    public static List<Vector3> airPath;
    public static EnemyManager singleton;
    public static int pathsUpdated = 0;

    [SerializeField]
    public float airHeight = 5;
    public float groundHeight = 0.1f;

    void Awake()
	{
		if (singleton != null) {
			Debug.LogError ("More than one Enemy manager is running"); 
		} else {
			singleton = this;
        }
    }
    public static float HorizontalDistance(Vector3 a, Vector3 b) {
        return Vector3.Distance(RemoveY(a), RemoveY(b));
    }
    public static int GetPathIndexFromPos(Vector3 pos, float dev, bool onGround) {
        if (onGround) {
            for (int i = 0; i < groundPath.Count; i++) {
                if (Vector3.Distance(groundPath[i], pos) < dev)
                    return i;
            }
        } else {
            for(int i = 0; i < airPath.Count; i++) {
                if (Vector3.Distance(airPath[i], pos) < dev)
                    return i;
            }
        }
        Debug.LogError("no mathing index for ground? answer: " + onGround);
        return 0;
    }
    //check line of sight along ground level (0.1f)
    public static bool CheckLineOfSight(Vector3 targetPos, List<Transform> edges, LayerMask mask) {
        foreach (Transform edge in edges) {
            Vector3 edgePos = GroundY(edge.position);
            targetPos = GroundY(targetPos);
            float dis = HorizontalDistance(targetPos, edgePos);
            Vector3 dir = targetPos - edgePos;
            dir.Normalize();
            RaycastHit hit;
            if (Physics.Raycast(edgePos, dir, out hit, dis, mask)) {
                if (hit.transform.tag == "Map") {
                    return false;
                }
            }
        }
        return true;
    }
    public static Vector3? GetClosestPathNodePos(Vector3 targetPos, List<Transform> edges, LayerMask mask, bool onGround) {//TODO make sure this doesnt take up too much processing power (if it does maybe try a different find function)
        if (onGround) {
            if (groundPath == new List<Vector3>() || groundPath == null) return new Vector3();
            targetPos = GroundY(targetPos);
        } else {
            if (airPath == new List<Vector3>() || airPath == null) return new Vector3();
            targetPos = AirY(targetPos);
        }
        Vector3? closest = null;
        float closestDis = 0;
        bool first = true;
        List<Vector3> path = onGround ? groundPath : airPath;
        foreach (Vector3 pn in path) {
            //TODO add fallback
            //closest = pn;
           
            float dis = HorizontalDistance(targetPos, pn);
            //check along ground
            if (CheckLineOfSight(pn, edges, mask)) {
                if (first) {
                    closest = pn;
                    closestDis = dis;
                    first = false;
                    continue;
                }
                if (dis < closestDis) {
                    closest = pn;
                    closestDis = dis;
                }
            }
        }
        return closest;
    }
    
    void Update() {
        pathsUpdated = 0;
    }
	
    public override void OnStartClient() {
        base.OnStartClient();
        if (!isServer) return;

        spawnableEnemies = new Dictionary<string, Enemy>();
        for (int i = 0; i < spawnableEnemyList.Count; i++) {
            string type = spawnableEnemyList[i].type;
            Enemy enemy = spawnableEnemyList[i];
            spawnableEnemies.Add(spawnableEnemyList[i].type, spawnableEnemyList[i]);
        }
        pathfinder = new Pathfinder();
        pathfinder = new Pathfinder();
        playerBase = GameObject.Find("EndZone").transform.position;
        spawnPos = GameObject.FindGameObjectWithTag("SpawnPos").transform.position;
        groundPath = pathfinder.FindVector3Path(spawnPos, playerBase, true, true);
        airPath = pathfinder.FindVector3Path(spawnPos, playerBase, true, false);
        if (groundPath.Count == 0) {
            Debug.LogError("Nopath");
        }
    }
    public static bool OnPath(Vector3 pos, float rad, bool forGround) {
        pos = forGround ? GroundY(pos) : AirY(pos);
        List<Vector3> path = forGround ? groundPath : airPath;
        foreach(Vector3 v3 in path) {
            if (Vector3.Distance(v3, pos) < rad)
                return true;
        }
        return false;
    }
    public bool UpdatePath() {
        List<Vector3> newPath = pathfinder.FindVector3Path(spawnPos, playerBase, true, true);
        if (newPath.Count == 0) {
            Debug.Log("Nopath");
            return false;
        }
        groundPath = newPath;
        return true;
    }
    //you can get a enemy from this getter
    public static Enemy GetEnemy(string _enemyID){
        if(enemies.ContainsKey(_enemyID))
		    return enemies[_enemyID];
        print("Couldn't find enemy with ID: " + _enemyID);
        return default(Enemy);
	}

	//Find the closest enemy to position
	public static Enemy GetClosestEnemy(Vector3 position){
		Enemy currentClosest = default(Enemy);
		float currentClosestDistance = 0f;
		foreach (Enemy enemy in enemies.Values) {
			float distance = Vector3.Distance (enemy.gameObject.transform.position, position);
			if(distance < currentClosestDistance || currentClosestDistance == 0f){
				currentClosest = enemy;
				currentClosestDistance = distance;
			}
		}
		if (currentClosest != default(Enemy)) {
			return currentClosest;
		}
		Debug.LogError ("No enemy found. Returned default enemy likely breaking script using this method");
		return default(Enemy);
	}


    Vector3 UpdateSpawnPos(Vector3 centreSpawnPos, float range) {
        Vector3 spawnPos;
        spawnPos.x = centreSpawnPos.x + (Random.Range(-range, range));
        spawnPos.z = centreSpawnPos.z + (Random.Range(-range, range));
        spawnPos.y = centreSpawnPos.y;
        return spawnPos;
    }
    public static Vector3 StandardY(Vector3 v, float height) {
        return new Vector3(v.x, height, v.z);
    }
    public static Vector3 GroundY(Vector3 v) {
        return StandardY(v, 0.1f);
    }
    public static Vector3 AirY(Vector3 v) {
        return StandardY(v, 4);
    }
    public static Vector3 RemoveY(Vector3 v) {
        return StandardY(v, 0);
    }

    #region Enemy Functions

    public void SpawnEnemy(string _type, Vector3 _pos, float _radius) {
        if (isServer) {
            Enemy enemy = EnemyManager.spawnableEnemies[_type];

            //Vector3 spawnPos = UpdateSpawnPos(_pos, _radius);
            Vector3 spawnPos = _pos;
            GameObject obj = enemy.gameObject;
            obj = (GameObject)Instantiate(obj, spawnPos, Quaternion.identity);

            NetworkServer.Spawn(obj);

            obj.transform.position = spawnPos;
            string ID = obj.GetComponent<NetworkIdentity>().netId.ToString();
            obj.name = _type + " " + ID;
            enemy = obj.GetComponent<Enemy>();
            EnemyManager.enemies.Add(obj.name, enemy);
        }
    }
    [Command]
    public void CmdDestroyEnemy(Vector3 _pos) {
        Enemy enemy = default(Enemy);
        string ID = "";
        foreach (KeyValuePair<string, Enemy> kp in EnemyManager.enemies) {
            if (kp.Value.transform.position == _pos) {
                enemy = kp.Value;
                ID = kp.Key;
                break;
            }
        }
        if (enemy != default(Enemy)) {
            EnemyManager.enemies.Remove(ID);
            NetworkServer.Destroy(enemy.gameObject);
        }
    }
    #endregion

    #region tempGridGizmosLocation
    void OnDrawGizmos() {
        if (!isServer) return;
        if (Grid.singleton == null)
            return;
        if (Grid.grid != null && GameObject.FindGameObjectsWithTag("Player").Length > 0) {
            //there are more than one players - how will this work with the Grid?
            List<GameObject> players = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));

            GridNode playerNode = Grid.GetNode(players[0].transform.position);

            foreach (GridNode gn in Grid.grid) {
                if (gn.flyable && gn.largeWalkable) {
                    Gizmos.color = Color.white;
                } else if (!gn.walkable) {
                    Gizmos.color = Color.red;
                } else if (!gn.largeWalkable) {
                    Gizmos.color = Color.yellow;
                }
                Gizmos.color = (playerNode == gn) ? Color.green : Gizmos.color;

                Gizmos.DrawWireCube(gn.worldPos, new Vector3(Grid.nodeDiameter, 1, Grid.nodeDiameter));

            }
            //foreach (Pathfinder.SearchNode sn in pathfinder.searchNodes) {
            //    if (sn.walkable && sn.largeWalkable) {
            //        Gizmos.color = Color.white;
            //    } else if (!sn.walkable) {
            //        Gizmos.color = Color.red;
            //    } else if (!sn.largeWalkable) {
            //        Gizmos.color = Color.yellow;
            //    }

            //    Gizmos.DrawWireCube(new Vector3(sn.position.x, 0, sn.position.y), new Vector3(Grid.nodeDiameter, 1, Grid.nodeDiameter));

            //}
            Vector3? prevV3 = null;
            Gizmos.color = Color.black;
            foreach (Vector3 v3 in EnemyManager.groundPath) {
                if (prevV3 != null)
                    Gizmos.DrawLine((Vector3)prevV3, v3);

                prevV3 = v3;
            }
            prevV3 = null;
            Gizmos.color = Color.magenta;
            foreach (Vector3 v3 in EnemyManager.airPath) {
                if (prevV3 != null)
                    Gizmos.DrawLine((Vector3)prevV3, v3);

                prevV3 = v3;
            }
        }
    }
    #endregion
}
