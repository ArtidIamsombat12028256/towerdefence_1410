﻿using UnityEngine;
using System.Collections;

public class MinimapScript : MonoBehaviour {


	private GameObject player; 

	// Use this for initialization
	void Start () {
		GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
		foreach(GameObject go in gos)
		{
			if(go.layer == 8 && go.CompareTag("Player"))//go.layer=="LocalPlayer" && go.CompareTag("Player"))
			{
				player = go; 
			}
		} 
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 playerPosition = player.transform.position;//GameObject.FindGameObjectWithTag ("Player").transform.position;
		this.transform.position = new Vector3 (playerPosition.x, 100, playerPosition.z);
	
	}
}
