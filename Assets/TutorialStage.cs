﻿using UnityEngine;
using System.Collections;

public delegate bool TutorialBoolean();

public class TutorialStage{
	private TutorialBoolean boolean;

	private string prompts;
	private string phasePrompt;

	public bool hasBeenCompleted;
	public bool hasShownPrompt;

	public bool IsCompleted(){
		return boolean.Invoke();
	}

	public TutorialStage(TutorialBoolean completeRequirements, string tutorialUIPrompt, string phasePrompt){
		boolean = completeRequirements;
		prompts = tutorialUIPrompt;
		this.phasePrompt = phasePrompt;
		hasBeenCompleted = false;
	}

	public string GetPrompts(){
		return prompts;
	}

	public string GetPhasePrompt(){
		return phasePrompt;
	}
}

